import Vue from 'vue'
import Router from 'vue-router'
import Layout from '@/components/Layout';
import Login from '@/pages/Login'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Layout',
      component: Layout,
      redirect: '/index',
      children: [
        { path: '/index', component: resolve => { require(['@/pages/Index'], resolve); } }
      ]
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    }
  ]
})
